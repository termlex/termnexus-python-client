# swagger_client.QueryresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_constraint_in_query_using_post**](QueryresourceApi.md#add_constraint_in_query_using_post) | **POST** /api/queries/{id}/constraints/included | Sets the included constraint for a query component expression.
[**add_contained_expressions_for_query_using_put**](QueryresourceApi.md#add_contained_expressions_for_query_using_put) | **PUT** /api/queries/{id}/children | adds child expressions to query with a given uuid
[**add_excluded_constraints_in_query_using_put**](QueryresourceApi.md#add_excluded_constraints_in_query_using_put) | **PUT** /api/queries/{id}/constraints/excluded | Adds the excluded constraints to a query component expression.
[**create_query_using_post**](QueryresourceApi.md#create_query_using_post) | **POST** /api/queries/{type} | creates a query based on the type requested (defaults to QueryComponent).
[**create_query_using_post1**](QueryresourceApi.md#create_query_using_post1) | **POST** /api/queries | creates a query based on the type requested (defaults to QueryComponent).
[**delete_constraint_in_query_component_using_delete**](QueryresourceApi.md#delete_constraint_in_query_component_using_delete) | **DELETE** /api/queries/{id}/constraints/included | Deletes the included constraint for a query component expression.
[**delete_query_using_delete**](QueryresourceApi.md#delete_query_using_delete) | **DELETE** /api/queries/{id} | deleteQuery
[**get_all_queries_using_get**](QueryresourceApi.md#get_all_queries_using_get) | **GET** /api/queries | getAllQueries
[**get_constraint_in_query_using_get**](QueryresourceApi.md#get_constraint_in_query_using_get) | **GET** /api/queries/{id}/constraints/included | Gets the included constraint for a query component expression.
[**get_constraints_in_query_using_get**](QueryresourceApi.md#get_constraints_in_query_using_get) | **GET** /api/queries/{id}/constraints | Gets the included and excluded constraints for a query component expression.
[**get_contained_expressions_for_query_using_get**](QueryresourceApi.md#get_contained_expressions_for_query_using_get) | **GET** /api/queries/{id}/children | returns contained child expressions for query with a given uuid
[**get_excluded_constraints_in_query_using_get**](QueryresourceApi.md#get_excluded_constraints_in_query_using_get) | **GET** /api/queries/{id}/constraints/excluded | Gets the excluded constraints for a query component expression.
[**get_query_using_get**](QueryresourceApi.md#get_query_using_get) | **GET** /api/queries/{id} | getQuery
[**get_subsumed_concepts_count_for_query_using_get**](QueryresourceApi.md#get_subsumed_concepts_count_for_query_using_get) | **GET** /api/queries/{id}/conceptcount | returns subsumed concepts count for query for a given uuid
[**get_subsumed_concepts_for_query_using_get**](QueryresourceApi.md#get_subsumed_concepts_for_query_using_get) | **GET** /api/queries/{id}/concepts | returns subsumed concepts for query for a given uuid
[**remove_contained_expressions_for_query_using_delete**](QueryresourceApi.md#remove_contained_expressions_for_query_using_delete) | **DELETE** /api/queries/{id}/children | removes child expressions from query with a given uuid
[**remove_excluded_constraints_in_query_using_delete**](QueryresourceApi.md#remove_excluded_constraints_in_query_using_delete) | **DELETE** /api/queries/{id}/constraints/excluded | Removes the excluded constraints from a query component expression.
[**set_contained_expressions_for_query_using_post**](QueryresourceApi.md#set_contained_expressions_for_query_using_post) | **POST** /api/queries/{id}/children | sets contained child expressions for query with a given uuid
[**set_excluded_constraints_in_query_using_post**](QueryresourceApi.md#set_excluded_constraints_in_query_using_post) | **POST** /api/queries/{id}/constraints/excluded | Sets the excluded constraints for a query component expression.
[**update_constraint_in_query_component_using_put**](QueryresourceApi.md#update_constraint_in_query_component_using_put) | **PUT** /api/queries/{id}/constraints/included | Updates the included constraint for a query component expression.
[**update_query_using_put**](QueryresourceApi.md#update_query_using_put) | **PUT** /api/queries/{id} | updates query with id


# **add_constraint_in_query_using_post**
> QueryExpression add_constraint_in_query_using_post(id, constraint)

Sets the included constraint for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
constraint = NULL # object | constraint

try: 
    # Sets the included constraint for a query component expression.
    api_response = api_instance.add_constraint_in_query_using_post(id, constraint)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->add_constraint_in_query_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **constraint** | **object**| constraint | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_contained_expressions_for_query_using_put**
> QueryExpression add_contained_expressions_for_query_using_put(id, contained_expressions_to_add)

adds child expressions to query with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
contained_expressions_to_add = [swagger_client.list[object]()] # list[object] | containedExpressionsToAdd

try: 
    # adds child expressions to query with a given uuid
    api_response = api_instance.add_contained_expressions_for_query_using_put(id, contained_expressions_to_add)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->add_contained_expressions_for_query_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **contained_expressions_to_add** | **list[object]**| containedExpressionsToAdd | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_excluded_constraints_in_query_using_put**
> QueryExpression add_excluded_constraints_in_query_using_put(id, constraints_to_add)

Adds the excluded constraints to a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
constraints_to_add = [swagger_client.list[object]()] # list[object] | constraintsToAdd

try: 
    # Adds the excluded constraints to a query component expression.
    api_response = api_instance.add_excluded_constraints_in_query_using_put(id, constraints_to_add)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->add_excluded_constraints_in_query_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **constraints_to_add** | **list[object]**| constraintsToAdd | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_query_using_post**
> QueryExpression create_query_using_post(type, contained_entities)

creates a query based on the type requested (defaults to QueryComponent).

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
type = 'type_example' # str | type
contained_entities = [swagger_client.list[object]()] # list[object] | containedEntities

try: 
    # creates a query based on the type requested (defaults to QueryComponent).
    api_response = api_instance.create_query_using_post(type, contained_entities)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->create_query_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| type | 
 **contained_entities** | **list[object]**| containedEntities | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_query_using_post1**
> QueryExpression create_query_using_post1(type=type, content=content)

creates a query based on the type requested (defaults to QueryComponent).

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
type = 'type_example' # str | type (optional)
content = NULL # object | content (optional)

try: 
    # creates a query based on the type requested (defaults to QueryComponent).
    api_response = api_instance.create_query_using_post1(type=type, content=content)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->create_query_using_post1: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| type | [optional] 
 **content** | **object**| content | [optional] 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_constraint_in_query_component_using_delete**
> QueryExpression delete_constraint_in_query_component_using_delete(id)

Deletes the included constraint for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # Deletes the included constraint for a query component expression.
    api_response = api_instance.delete_constraint_in_query_component_using_delete(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->delete_constraint_in_query_component_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_query_using_delete**
> delete_query_using_delete(id)

deleteQuery

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # deleteQuery
    api_instance.delete_query_using_delete(id)
except ApiException as e:
    print "Exception when calling QueryresourceApi->delete_query_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_queries_using_get**
> list[QueryExpression] get_all_queries_using_get()

getAllQueries

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()

try: 
    # getAllQueries
    api_response = api_instance.get_all_queries_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_all_queries_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[QueryExpression]**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_constraint_in_query_using_get**
> TerminologyConstraint get_constraint_in_query_using_get(id)

Gets the included constraint for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # Gets the included constraint for a query component expression.
    api_response = api_instance.get_constraint_in_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_constraint_in_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_constraints_in_query_using_get**
> object get_constraints_in_query_using_get(id)

Gets the included and excluded constraints for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # Gets the included and excluded constraints for a query component expression.
    api_response = api_instance.get_constraints_in_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_constraints_in_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_contained_expressions_for_query_using_get**
> list[object] get_contained_expressions_for_query_using_get(id)

returns contained child expressions for query with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # returns contained child expressions for query with a given uuid
    api_response = api_instance.get_contained_expressions_for_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_contained_expressions_for_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_excluded_constraints_in_query_using_get**
> list[object] get_excluded_constraints_in_query_using_get(id)

Gets the excluded constraints for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # Gets the excluded constraints for a query component expression.
    api_response = api_instance.get_excluded_constraints_in_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_excluded_constraints_in_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_query_using_get**
> QueryExpression get_query_using_get(id)

getQuery

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # getQuery
    api_response = api_instance.get_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumed_concepts_count_for_query_using_get**
> list[object] get_subsumed_concepts_count_for_query_using_get(id)

returns subsumed concepts count for query for a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # returns subsumed concepts count for query for a given uuid
    api_response = api_instance.get_subsumed_concepts_count_for_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_subsumed_concepts_count_for_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumed_concepts_for_query_using_get**
> list[object] get_subsumed_concepts_for_query_using_get(id)

returns subsumed concepts for query for a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id

try: 
    # returns subsumed concepts for query for a given uuid
    api_response = api_instance.get_subsumed_concepts_for_query_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->get_subsumed_concepts_for_query_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_contained_expressions_for_query_using_delete**
> QueryExpression remove_contained_expressions_for_query_using_delete(id, contained_expressions)

removes child expressions from query with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
contained_expressions = [swagger_client.list[object]()] # list[object] | containedExpressions

try: 
    # removes child expressions from query with a given uuid
    api_response = api_instance.remove_contained_expressions_for_query_using_delete(id, contained_expressions)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->remove_contained_expressions_for_query_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **contained_expressions** | **list[object]**| containedExpressions | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_excluded_constraints_in_query_using_delete**
> QueryExpression remove_excluded_constraints_in_query_using_delete(id, excluded_constraints)

Removes the excluded constraints from a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
excluded_constraints = [swagger_client.list[object]()] # list[object] | excludedConstraints

try: 
    # Removes the excluded constraints from a query component expression.
    api_response = api_instance.remove_excluded_constraints_in_query_using_delete(id, excluded_constraints)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->remove_excluded_constraints_in_query_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **excluded_constraints** | **list[object]**| excludedConstraints | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_contained_expressions_for_query_using_post**
> QueryExpression set_contained_expressions_for_query_using_post(id, contained_expressions)

sets contained child expressions for query with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
contained_expressions = [swagger_client.list[object]()] # list[object] | containedExpressions

try: 
    # sets contained child expressions for query with a given uuid
    api_response = api_instance.set_contained_expressions_for_query_using_post(id, contained_expressions)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->set_contained_expressions_for_query_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **contained_expressions** | **list[object]**| containedExpressions | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_excluded_constraints_in_query_using_post**
> QueryExpression set_excluded_constraints_in_query_using_post(id, excluded_constraints)

Sets the excluded constraints for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
excluded_constraints = [swagger_client.Map()] # list[Map] | excludedConstraints

try: 
    # Sets the excluded constraints for a query component expression.
    api_response = api_instance.set_excluded_constraints_in_query_using_post(id, excluded_constraints)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->set_excluded_constraints_in_query_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **excluded_constraints** | [**list[Map]**](Map.md)| excludedConstraints | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_constraint_in_query_component_using_put**
> QueryExpression update_constraint_in_query_component_using_put(id, included_id, subsumption_flavour=subsumption_flavour)

Updates the included constraint for a query component expression.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
included_id = 'included_id_example' # str | includedId
subsumption_flavour = 'subsumption_flavour_example' # str | subsumptionFlavour (optional)

try: 
    # Updates the included constraint for a query component expression.
    api_response = api_instance.update_constraint_in_query_component_using_put(id, included_id, subsumption_flavour=subsumption_flavour)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->update_constraint_in_query_component_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **included_id** | **str**| includedId | 
 **subsumption_flavour** | **str**| subsumptionFlavour | [optional] 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_query_using_put**
> QueryExpression update_query_using_put(id, content)

updates query with id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.QueryresourceApi()
id = 'id_example' # str | id
content = NULL # object | content

try: 
    # updates query with id
    api_response = api_instance.update_query_using_put(id, content)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling QueryresourceApi->update_query_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **content** | **object**| content | 

### Return type

[**QueryExpression**](QueryExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

