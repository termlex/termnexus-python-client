# swagger_client.ConstraintresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_constraint_using_post**](ConstraintresourceApi.md#create_constraint_using_post) | **POST** /api/constraints/{id} | creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**delete_constraint_using_delete**](ConstraintresourceApi.md#delete_constraint_using_delete) | **DELETE** /api/constraints/{id} | deleteConstraint
[**get_all_constraints_using_get**](ConstraintresourceApi.md#get_all_constraints_using_get) | **GET** /api/constraints | getAllConstraints
[**get_constraint_using_get**](ConstraintresourceApi.md#get_constraint_using_get) | **GET** /api/constraints/{id} | getConstraint
[**get_subsumed_concept_count_for_constraint_using_get**](ConstraintresourceApi.md#get_subsumed_concept_count_for_constraint_using_get) | **GET** /api/constraints/{id}/conceptcount | returns subsumed concepts count for constraint with a given uuid
[**get_subsumed_concepts_for_constraint_using_get**](ConstraintresourceApi.md#get_subsumed_concepts_for_constraint_using_get) | **GET** /api/constraints/{id}/concepts | returns subsumed concepts for constraint with a given uuid
[**get_subsumption_for_constraint_using_get**](ConstraintresourceApi.md#get_subsumption_for_constraint_using_get) | **GET** /api/constraints/{id}/flavour | gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**set_subsumption_for_constraint_using_put**](ConstraintresourceApi.md#set_subsumption_for_constraint_using_put) | **PUT** /api/constraints/{id}/flavour | updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
[**update_constraint_using_put**](ConstraintresourceApi.md#update_constraint_using_put) | **PUT** /api/constraints/{id} | updates constraint with id


# **create_constraint_using_post**
> TerminologyConstraint create_constraint_using_post(id, flavour=flavour)

creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id
flavour = {'key': 'flavour_example'} # dict(str, str) | flavour (optional)

try: 
    # creates a query based on the conceptId and subsumption flavour (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
    api_response = api_instance.create_constraint_using_post(id, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->create_constraint_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **flavour** | [**dict(str, str)**](str.md)| flavour | [optional] 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_constraint_using_delete**
> delete_constraint_using_delete(id)

deleteConstraint

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id

try: 
    # deleteConstraint
    api_instance.delete_constraint_using_delete(id)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->delete_constraint_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_constraints_using_get**
> list[TerminologyConstraint] get_all_constraints_using_get()

getAllConstraints

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()

try: 
    # getAllConstraints
    api_response = api_instance.get_all_constraints_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->get_all_constraints_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[TerminologyConstraint]**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_constraint_using_get**
> TerminologyConstraint get_constraint_using_get(id)

getConstraint

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id

try: 
    # getConstraint
    api_response = api_instance.get_constraint_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->get_constraint_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumed_concept_count_for_constraint_using_get**
> list[object] get_subsumed_concept_count_for_constraint_using_get(id)

returns subsumed concepts count for constraint with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id

try: 
    # returns subsumed concepts count for constraint with a given uuid
    api_response = api_instance.get_subsumed_concept_count_for_constraint_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->get_subsumed_concept_count_for_constraint_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumed_concepts_for_constraint_using_get**
> list[object] get_subsumed_concepts_for_constraint_using_get(id)

returns subsumed concepts for constraint with a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id

try: 
    # returns subsumed concepts for constraint with a given uuid
    api_response = api_instance.get_subsumed_concepts_for_constraint_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->get_subsumed_concepts_for_constraint_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumption_for_constraint_using_get**
> TerminologyConstraint get_subsumption_for_constraint_using_get(id)

gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id

try: 
    # gets the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
    api_response = api_instance.get_subsumption_for_constraint_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->get_subsumption_for_constraint_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_subsumption_for_constraint_using_put**
> TerminologyConstraint set_subsumption_for_constraint_using_put(id, flavour)

updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id
flavour = NULL # object | flavour

try: 
    # updates the subsumption flavour for constraint with id (defaults to SubsumptionFlavour.SELF_OR_ANY_TYPE_OF).
    api_response = api_instance.set_subsumption_for_constraint_using_put(id, flavour)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->set_subsumption_for_constraint_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **flavour** | **object**| flavour | 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_constraint_using_put**
> TerminologyConstraint update_constraint_using_put(id, content)

updates constraint with id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConstraintresourceApi()
id = 'id_example' # str | id
content = NULL # object | content

try: 
    # updates constraint with id
    api_response = api_instance.update_constraint_using_put(id, content)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConstraintresourceApi->update_constraint_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **content** | **object**| content | 

### Return type

[**TerminologyConstraint**](TerminologyConstraint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

