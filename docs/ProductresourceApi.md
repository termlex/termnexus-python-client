# swagger_client.ProductresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_product_using_post**](ProductresourceApi.md#create_product_using_post) | **POST** /api/products | createProduct
[**delete_product_using_delete**](ProductresourceApi.md#delete_product_using_delete) | **DELETE** /api/products/{id} | deleteProduct
[**get_all_products_using_get**](ProductresourceApi.md#get_all_products_using_get) | **GET** /api/products | getAllProducts
[**get_product_using_get**](ProductresourceApi.md#get_product_using_get) | **GET** /api/products/{id} | getProduct
[**get_product_versions_using_get**](ProductresourceApi.md#get_product_versions_using_get) | **GET** /api/products/{id}/versions | getProductVersions
[**update_product_using_put**](ProductresourceApi.md#update_product_using_put) | **PUT** /api/products | updateProduct


# **create_product_using_post**
> CodeSystem create_product_using_post(product)

createProduct

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()
product = swagger_client.CodeSystem() # CodeSystem | product

try: 
    # createProduct
    api_response = api_instance.create_product_using_post(product)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ProductresourceApi->create_product_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**CodeSystem**](CodeSystem.md)| product | 

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_product_using_delete**
> delete_product_using_delete(id)

deleteProduct

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()
id = 'id_example' # str | id

try: 
    # deleteProduct
    api_instance.delete_product_using_delete(id)
except ApiException as e:
    print "Exception when calling ProductresourceApi->delete_product_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_products_using_get**
> list[CodeSystem] get_all_products_using_get()

getAllProducts

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()

try: 
    # getAllProducts
    api_response = api_instance.get_all_products_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ProductresourceApi->get_all_products_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[CodeSystem]**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_product_using_get**
> CodeSystem get_product_using_get(id)

getProduct

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()
id = 'id_example' # str | id

try: 
    # getProduct
    api_response = api_instance.get_product_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ProductresourceApi->get_product_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_product_versions_using_get**
> list[Version] get_product_versions_using_get(id)

getProductVersions

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()
id = 'id_example' # str | id

try: 
    # getProductVersions
    api_response = api_instance.get_product_versions_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ProductresourceApi->get_product_versions_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**list[Version]**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_product_using_put**
> CodeSystem update_product_using_put(object)

updateProduct

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProductresourceApi()
object = NULL # object | object

try: 
    # updateProduct
    api_response = api_instance.update_product_using_put(object)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ProductresourceApi->update_product_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **object** | **object**| object | 

### Return type

[**CodeSystem**](CodeSystem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

