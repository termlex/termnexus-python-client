# swagger_client.ConceptresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_concept_using_post**](ConceptresourceApi.md#create_concept_using_post) | **POST** /api/concepts | createConcept
[**delete_concept_using_delete**](ConceptresourceApi.md#delete_concept_using_delete) | **DELETE** /api/concepts/{id} | deleteConcept
[**get_all_concepts_using_get**](ConceptresourceApi.md#get_all_concepts_using_get) | **GET** /api/concepts | getAllConcepts
[**get_component_status_for_id_using_get**](ConceptresourceApi.md#get_component_status_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/status | get the status of concept with given id
[**get_compositional_grammar_form_for_id_using_get**](ConceptresourceApi.md#get_compositional_grammar_form_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/cgf | get the compositional grammar form of concept with given id
[**get_concept_for_id_using_get**](ConceptresourceApi.md#get_concept_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id} | get the concept with given id
[**get_concept_fully_specified_name_for_id_using_get**](ConceptresourceApi.md#get_concept_fully_specified_name_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/fsn | get the Fully Specified Name of concept with given id and language
[**get_concept_preferred_term_for_id_using_get**](ConceptresourceApi.md#get_concept_preferred_term_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/pt | get the Preferred Term of concept with given id
[**get_concept_type_for_id_using_get**](ConceptresourceApi.md#get_concept_type_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/type | get the type of concept with given id
[**get_concept_using_get**](ConceptresourceApi.md#get_concept_using_get) | **GET** /api/concepts/{id} | getConcept
[**get_delta_content_using_get**](ConceptresourceApi.md#get_delta_content_using_get) | **GET** /api/concepts/version/{version}/delta/all | get all delta content for the release
[**get_long_normal_form_for_id_using_get**](ConceptresourceApi.md#get_long_normal_form_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/lnf | get the long normal form of concept with given id
[**get_newly_added_content_using_get**](ConceptresourceApi.md#get_newly_added_content_using_get) | **GET** /api/concepts/version/{version}/delta/new | get newly added content for the release
[**get_newly_modified_content_using_get**](ConceptresourceApi.md#get_newly_modified_content_using_get) | **GET** /api/concepts/version/{version}/delta/modified | get still active concepts modified in the release
[**get_newly_resurrected_content_using_get**](ConceptresourceApi.md#get_newly_resurrected_content_using_get) | **GET** /api/concepts/version/{version}/delta/resurrected | get newly resurrected content for the release
[**get_newly_retired_content_using_get**](ConceptresourceApi.md#get_newly_retired_content_using_get) | **GET** /api/concepts/version/{version}/delta/retired | get newly retired content for the release
[**get_prospective_replacements_for_id_using_get**](ConceptresourceApi.md#get_prospective_replacements_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/history/prospective | get the prospective replacement map for concept with given id
[**get_retrospective_replacements_for_id_using_get**](ConceptresourceApi.md#get_retrospective_replacements_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/history/retroprospective | get the retrospective replacement map concept with given id
[**get_short_normal_form_for_id_using_get**](ConceptresourceApi.md#get_short_normal_form_for_id_using_get) | **GET** /api/concepts/version/{version}/id/{id}/snf | get the short normal form of concept with given id
[**get_subsumption_relationship_using_get**](ConceptresourceApi.md#get_subsumption_relationship_using_get) | **GET** /api/concepts/compare | get the subsumption relationship of comparing two concepts
[**update_concept_using_put**](ConceptresourceApi.md#update_concept_using_put) | **PUT** /api/concepts | updateConcept


# **create_concept_using_post**
> TerminologyConceptImpl create_concept_using_post(concept)

createConcept

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
concept = swagger_client.TerminologyConceptImpl() # TerminologyConceptImpl | concept

try: 
    # createConcept
    api_response = api_instance.create_concept_using_post(concept)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->create_concept_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **concept** | [**TerminologyConceptImpl**](TerminologyConceptImpl.md)| concept | 

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_concept_using_delete**
> delete_concept_using_delete(id)

deleteConcept

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 'id_example' # str | id

try: 
    # deleteConcept
    api_instance.delete_concept_using_delete(id)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->delete_concept_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_concepts_using_get**
> list[TerminologyConceptImpl] get_all_concepts_using_get()

getAllConcepts

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()

try: 
    # getAllConcepts
    api_response = api_instance.get_all_concepts_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_all_concepts_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[TerminologyConceptImpl]**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_component_status_for_id_using_get**
> str get_component_status_for_id_using_get(id, version)

get the status of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the status of concept with given id
    api_response = api_instance.get_component_status_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_component_status_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_compositional_grammar_form_for_id_using_get**
> str get_compositional_grammar_form_for_id_using_get(id, version)

get the compositional grammar form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the compositional grammar form of concept with given id
    api_response = api_instance.get_compositional_grammar_form_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_compositional_grammar_form_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concept_for_id_using_get**
> SnomedConcept get_concept_for_id_using_get(id, version, flavour=flavour)

get the concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string
flavour = 'flavour_example' # str | The flavour of the concept - specifies details retrieved (optional)

try: 
    # get the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 
 **flavour** | **str**| The flavour of the concept - specifies details retrieved | [optional] 

### Return type

[**SnomedConcept**](SnomedConcept.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concept_fully_specified_name_for_id_using_get**
> str get_concept_fully_specified_name_for_id_using_get(id, language, version)

get the Fully Specified Name of concept with given id and language

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 'id_example' # str | id
language = 'language_example' # str | language
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the Fully Specified Name of concept with given id and language
    api_response = api_instance.get_concept_fully_specified_name_for_id_using_get(id, language, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_concept_fully_specified_name_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **language** | **str**| language | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concept_preferred_term_for_id_using_get**
> str get_concept_preferred_term_for_id_using_get(id, language, version)

get the Preferred Term of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 'id_example' # str | id
language = 'language_example' # str | language
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the Preferred Term of concept with given id
    api_response = api_instance.get_concept_preferred_term_for_id_using_get(id, language, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_concept_preferred_term_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **language** | **str**| language | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concept_type_for_id_using_get**
> str get_concept_type_for_id_using_get(id, version)

get the type of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the type of concept with given id
    api_response = api_instance.get_concept_type_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_concept_type_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concept_using_get**
> TerminologyConceptImpl get_concept_using_get(id)

getConcept

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 'id_example' # str | id

try: 
    # getConcept
    api_response = api_instance.get_concept_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_concept_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delta_content_using_get**
> list[object] get_delta_content_using_get(version)

get all delta content for the release

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get all delta content for the release
    api_response = api_instance.get_delta_content_using_get(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_delta_content_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_long_normal_form_for_id_using_get**
> NormalFormExpression get_long_normal_form_for_id_using_get(id, version)

get the long normal form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the long normal form of concept with given id
    api_response = api_instance.get_long_normal_form_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_long_normal_form_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_newly_added_content_using_get**
> list[object] get_newly_added_content_using_get(version)

get newly added content for the release

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get newly added content for the release
    api_response = api_instance.get_newly_added_content_using_get(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_newly_added_content_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_newly_modified_content_using_get**
> list[object] get_newly_modified_content_using_get(version)

get still active concepts modified in the release

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get still active concepts modified in the release
    api_response = api_instance.get_newly_modified_content_using_get(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_newly_modified_content_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_newly_resurrected_content_using_get**
> list[object] get_newly_resurrected_content_using_get(version)

get newly resurrected content for the release

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get newly resurrected content for the release
    api_response = api_instance.get_newly_resurrected_content_using_get(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_newly_resurrected_content_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_newly_retired_content_using_get**
> list[object] get_newly_retired_content_using_get(version)

get newly retired content for the release

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get newly retired content for the release
    api_response = api_instance.get_newly_retired_content_using_get(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_newly_retired_content_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_prospective_replacements_for_id_using_get**
> object get_prospective_replacements_for_id_using_get(id, version)

get the prospective replacement map for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the prospective replacement map for concept with given id
    api_response = api_instance.get_prospective_replacements_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_prospective_replacements_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_retrospective_replacements_for_id_using_get**
> object get_retrospective_replacements_for_id_using_get(id, version)

get the retrospective replacement map concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the retrospective replacement map concept with given id
    api_response = api_instance.get_retrospective_replacements_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_retrospective_replacements_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_short_normal_form_for_id_using_get**
> NormalFormExpression get_short_normal_form_for_id_using_get(id, version)

get the short normal form of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
id = 789 # int | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the short normal form of concept with given id
    api_response = api_instance.get_short_normal_form_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_short_normal_form_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumption_relationship_using_get**
> str get_subsumption_relationship_using_get(concept_id1, concept_id2, version=version)

get the subsumption relationship of comparing two concepts

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
concept_id1 = 'concept_id1_example' # str | conceptId1
concept_id2 = 'concept_id2_example' # str | conceptId2
version = 'version_example' # str | The version of the terminology product as date string (optional)

try: 
    # get the subsumption relationship of comparing two concepts
    api_response = api_instance.get_subsumption_relationship_using_get(concept_id1, concept_id2, version=version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->get_subsumption_relationship_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **concept_id1** | **str**| conceptId1 | 
 **concept_id2** | **str**| conceptId2 | 
 **version** | **str**| The version of the terminology product as date string | [optional] 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_concept_using_put**
> TerminologyConceptImpl update_concept_using_put(concept)

updateConcept

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConceptresourceApi()
concept = swagger_client.TerminologyConceptImpl() # TerminologyConceptImpl | concept

try: 
    # updateConcept
    api_response = api_instance.update_concept_using_put(concept)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ConceptresourceApi->update_concept_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **concept** | [**TerminologyConceptImpl**](TerminologyConceptImpl.md)| concept | 

### Return type

[**TerminologyConceptImpl**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

