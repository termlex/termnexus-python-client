# Expression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**child_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**equivalent_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**id** | **str** |  | [optional] 
**parent_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


