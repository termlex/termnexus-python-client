# swagger_client.RefsetresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_definitions_using_put**](RefsetresourceApi.md#add_definitions_using_put) | **PUT** /api/refsets/{id}/definitions | adds definitions to refset with given id. This does not clear existing definitions.
[**add_members_using_put**](RefsetresourceApi.md#add_members_using_put) | **PUT** /api/refsets/{id}/members | adds collection as members of refset with given id. This does not clear existing members.
[**create_definitions_using_post**](RefsetresourceApi.md#create_definitions_using_post) | **POST** /api/refsets/{id}/definitions | sets collection as definitions of refset with given id. Note this clears existing definitions.
[**create_members_using_post**](RefsetresourceApi.md#create_members_using_post) | **POST** /api/refsets/{id}/members | set collection as members of refset with given id. Note this clears existing members.
[**create_refset_using_post**](RefsetresourceApi.md#create_refset_using_post) | **POST** /api/refsets | createRefset
[**delete_refset_using_delete**](RefsetresourceApi.md#delete_refset_using_delete) | **DELETE** /api/refsets/{id} | deleteRefset
[**get_all_refsets_using_get**](RefsetresourceApi.md#get_all_refsets_using_get) | **GET** /api/refsets | getAllRefsets
[**get_refset_by_sct_id_using_get**](RefsetresourceApi.md#get_refset_by_sct_id_using_get) | **GET** /api/refsets/sct/{id} | returns refset for a given SNOMED CT id
[**get_refset_by_uuid_using_get**](RefsetresourceApi.md#get_refset_by_uuid_using_get) | **GET** /api/refsets/uuid/{id} | returns refset for a given uuid
[**get_refset_definitions_using_get**](RefsetresourceApi.md#get_refset_definitions_using_get) | **GET** /api/refsets/{id}/definitions | returns definitions of refset with given id
[**get_refset_member_count_using_get**](RefsetresourceApi.md#get_refset_member_count_using_get) | **GET** /api/refsets/{id}/members/count | returns member count for refset with given id
[**get_refset_members_using_get**](RefsetresourceApi.md#get_refset_members_using_get) | **GET** /api/refsets/{id}/members | returns members of refset with given id
[**get_refset_using_get**](RefsetresourceApi.md#get_refset_using_get) | **GET** /api/refsets/{id} | getRefset
[**get_refsets_in_module_using_get**](RefsetresourceApi.md#get_refsets_in_module_using_get) | **GET** /api/refsets/module/{id} | gets all refsets that belong to module with a given id
[**remove_definitions_using_delete**](RefsetresourceApi.md#remove_definitions_using_delete) | **DELETE** /api/refsets/{id}/definitions | deletes definitions from refset with given id
[**remove_members_using_delete**](RefsetresourceApi.md#remove_members_using_delete) | **DELETE** /api/refsets/{id}/members | removes collection from members of refset with given id
[**search_refset_using_get**](RefsetresourceApi.md#search_refset_using_get) | **GET** /api/refsets/{id}/version/{version}/search | Returns matches in a refset for given token
[**update_refset_using_put**](RefsetresourceApi.md#update_refset_using_put) | **PUT** /api/refsets | updateRefset


# **add_definitions_using_put**
> Refset add_definitions_using_put(id, definitions)

adds definitions to refset with given id. This does not clear existing definitions.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
definitions = [swagger_client.list[object]()] # list[object] | definitions

try: 
    # adds definitions to refset with given id. This does not clear existing definitions.
    api_response = api_instance.add_definitions_using_put(id, definitions)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->add_definitions_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **definitions** | **list[object]**| definitions | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_members_using_put**
> Refset add_members_using_put(id, members)

adds collection as members of refset with given id. This does not clear existing members.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
members = [swagger_client.list[str]()] # list[str] | members

try: 
    # adds collection as members of refset with given id. This does not clear existing members.
    api_response = api_instance.add_members_using_put(id, members)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->add_members_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **members** | **list[str]**| members | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_definitions_using_post**
> Refset create_definitions_using_post(id, definitions)

sets collection as definitions of refset with given id. Note this clears existing definitions.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
definitions = [swagger_client.list[object]()] # list[object] | definitions

try: 
    # sets collection as definitions of refset with given id. Note this clears existing definitions.
    api_response = api_instance.create_definitions_using_post(id, definitions)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->create_definitions_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **definitions** | **list[object]**| definitions | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_members_using_post**
> Refset create_members_using_post(id, members)

set collection as members of refset with given id. Note this clears existing members.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
members = [swagger_client.list[str]()] # list[str] | members

try: 
    # set collection as members of refset with given id. Note this clears existing members.
    api_response = api_instance.create_members_using_post(id, members)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->create_members_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **members** | **list[str]**| members | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_refset_using_post**
> Refset create_refset_using_post(refset)

createRefset

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
refset = swagger_client.Refset() # Refset | refset

try: 
    # createRefset
    api_response = api_instance.create_refset_using_post(refset)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->create_refset_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refset** | [**Refset**](Refset.md)| refset | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_refset_using_delete**
> delete_refset_using_delete(id)

deleteRefset

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # deleteRefset
    api_instance.delete_refset_using_delete(id)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->delete_refset_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_refsets_using_get**
> list[Refset] get_all_refsets_using_get()

getAllRefsets

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()

try: 
    # getAllRefsets
    api_response = api_instance.get_all_refsets_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_all_refsets_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Refset]**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_by_sct_id_using_get**
> Refset get_refset_by_sct_id_using_get(id)

returns refset for a given SNOMED CT id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 789 # int | id

try: 
    # returns refset for a given SNOMED CT id
    api_response = api_instance.get_refset_by_sct_id_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_by_sct_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_by_uuid_using_get**
> Refset get_refset_by_uuid_using_get(id)

returns refset for a given uuid

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # returns refset for a given uuid
    api_response = api_instance.get_refset_by_uuid_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_by_uuid_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_definitions_using_get**
> list[object] get_refset_definitions_using_get(id)

returns definitions of refset with given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # returns definitions of refset with given id
    api_response = api_instance.get_refset_definitions_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_definitions_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_member_count_using_get**
> int get_refset_member_count_using_get(id)

returns member count for refset with given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # returns member count for refset with given id
    api_response = api_instance.get_refset_member_count_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_member_count_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_members_using_get**
> Refset get_refset_members_using_get(id)

returns members of refset with given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # returns members of refset with given id
    api_response = api_instance.get_refset_members_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_members_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refset_using_get**
> Refset get_refset_using_get(id)

getRefset

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id

try: 
    # getRefset
    api_response = api_instance.get_refset_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refset_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_refsets_in_module_using_get**
> list[object] get_refsets_in_module_using_get(id)

gets all refsets that belong to module with a given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 789 # int | id

try: 
    # gets all refsets that belong to module with a given id
    api_response = api_instance.get_refsets_in_module_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->get_refsets_in_module_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_definitions_using_delete**
> Refset remove_definitions_using_delete(id, definitions)

deletes definitions from refset with given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
definitions = [swagger_client.list[object]()] # list[object] | definitions

try: 
    # deletes definitions from refset with given id
    api_response = api_instance.remove_definitions_using_delete(id, definitions)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->remove_definitions_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **definitions** | **list[object]**| definitions | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_members_using_delete**
> Refset remove_members_using_delete(id, members)

removes collection from members of refset with given id

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
id = 'id_example' # str | id
members = [swagger_client.list[str]()] # list[str] | members

try: 
    # removes collection from members of refset with given id
    api_response = api_instance.remove_members_using_delete(id, members)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->remove_members_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **members** | **list[str]**| members | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_refset_using_get**
> object search_refset_using_get(term, id, version, language_code=language_code)

Returns matches in a refset for given token

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
term = 'term_example' # str | Token to search for; can be a term or an id
id = 'id_example' # str | The id of the refset
version = 'version_example' # str | The version of the terminology product as date string. Defaults to default version specified in server instance
language_code = 'en' # str | The language of descriptions to search (optional) (default to en)

try: 
    # Returns matches in a refset for given token
    api_response = api_instance.search_refset_using_get(term, id, version, language_code=language_code)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->search_refset_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **str**| Token to search for; can be a term or an id | 
 **id** | **str**| The id of the refset | 
 **version** | **str**| The version of the terminology product as date string. Defaults to default version specified in server instance | 
 **language_code** | **str**| The language of descriptions to search | [optional] [default to en]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_refset_using_put**
> Refset update_refset_using_put(refset)

updateRefset

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RefsetresourceApi()
refset = swagger_client.Refset() # Refset | refset

try: 
    # updateRefset
    api_response = api_instance.update_refset_using_put(refset)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling RefsetresourceApi->update_refset_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refset** | [**Refset**](Refset.md)| refset | 

### Return type

[**Refset**](Refset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

