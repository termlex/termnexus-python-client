# SnomedRoleGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relationship_group_id** | **str** |  | [optional] 
**relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


