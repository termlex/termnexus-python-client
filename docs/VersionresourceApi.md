# swagger_client.VersionresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_concepts_to_version_using_post**](VersionresourceApi.md#add_concepts_to_version_using_post) | **POST** /api/versions/{id}/import | addConceptsToVersion
[**create_version_using_post**](VersionresourceApi.md#create_version_using_post) | **POST** /api/versions | createVersion
[**delete_version_using_delete**](VersionresourceApi.md#delete_version_using_delete) | **DELETE** /api/versions/{id} | deleteVersion
[**get_all_versions_using_get**](VersionresourceApi.md#get_all_versions_using_get) | **GET** /api/versions | getAllVersions
[**get_concepts_for_version_using_get**](VersionresourceApi.md#get_concepts_for_version_using_get) | **GET** /api/versions/{id}/concepts | getConceptsForVersion
[**get_default_version_using_get**](VersionresourceApi.md#get_default_version_using_get) | **GET** /api/versions/default | getDefaultVersion
[**get_version_using_get**](VersionresourceApi.md#get_version_using_get) | **GET** /api/versions/{id} | getVersion
[**set_default_version_using_put**](VersionresourceApi.md#set_default_version_using_put) | **PUT** /api/versions/default | setDefaultVersion
[**update_version_using_put**](VersionresourceApi.md#update_version_using_put) | **PUT** /api/versions | updateVersion


# **add_concepts_to_version_using_post**
> list[TerminologyConceptImpl] add_concepts_to_version_using_post(id, concepts)

addConceptsToVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
id = 'id_example' # str | The version of the terminology product as date string
concepts = [swagger_client.Map()] # list[Map] | concepts

try: 
    # addConceptsToVersion
    api_response = api_instance.add_concepts_to_version_using_post(id, concepts)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->add_concepts_to_version_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The version of the terminology product as date string | 
 **concepts** | [**list[Map]**](Map.md)| concepts | 

### Return type

[**list[TerminologyConceptImpl]**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_version_using_post**
> Version create_version_using_post(version)

createVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
version = swagger_client.Version() # Version | version

try: 
    # createVersion
    api_response = api_instance.create_version_using_post(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->create_version_using_post: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | [**Version**](Version.md)| version | 

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_version_using_delete**
> delete_version_using_delete(id)

deleteVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
id = 'id_example' # str | id

try: 
    # deleteVersion
    api_instance.delete_version_using_delete(id)
except ApiException as e:
    print "Exception when calling VersionresourceApi->delete_version_using_delete: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_versions_using_get**
> list[Version] get_all_versions_using_get()

getAllVersions

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()

try: 
    # getAllVersions
    api_response = api_instance.get_all_versions_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->get_all_versions_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Version]**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_concepts_for_version_using_get**
> list[TerminologyConceptImpl] get_concepts_for_version_using_get(id)

getConceptsForVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
id = 'id_example' # str | The version of the terminology product as date string

try: 
    # getConceptsForVersion
    api_response = api_instance.get_concepts_for_version_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->get_concepts_for_version_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The version of the terminology product as date string | 

### Return type

[**list[TerminologyConceptImpl]**](TerminologyConceptImpl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_default_version_using_get**
> Version get_default_version_using_get()

getDefaultVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()

try: 
    # getDefaultVersion
    api_response = api_instance.get_default_version_using_get()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->get_default_version_using_get: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_version_using_get**
> Version get_version_using_get(id)

getVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
id = 'id_example' # str | id

try: 
    # getVersion
    api_response = api_instance.get_version_using_get(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->get_version_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_default_version_using_put**
> Version set_default_version_using_put(id)

setDefaultVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
id = 'id_example' # str | id

try: 
    # setDefaultVersion
    api_response = api_instance.set_default_version_using_put(id)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->set_default_version_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_version_using_put**
> Version update_version_using_put(version)

updateVersion

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VersionresourceApi()
version = swagger_client.Version() # Version | version

try: 
    # updateVersion
    api_response = api_instance.update_version_using_put(version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling VersionresourceApi->update_version_using_put: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | [**Version**](Version.md)| version | 

### Return type

[**Version**](Version.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

