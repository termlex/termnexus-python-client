# Calendar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calendar_type** | **str** |  | [optional] 
**fields_computed** | **int** |  | [optional] 
**fields_normalized** | **int** |  | [optional] 
**first_day_of_week** | **int** |  | [optional] 
**lenient** | **bool** |  | [optional] 
**minimal_days_in_first_week** | **int** |  | [optional] 
**time** | **datetime** |  | [optional] 
**time_in_millis** | **int** |  | [optional] 
**time_zone** | [**TimeZone**](TimeZone.md) |  | [optional] 
**week_count_data** | [**Locale**](Locale.md) |  | [optional] 
**week_date_supported** | **bool** |  | [optional] 
**week_year** | **int** |  | [optional] 
**weeks_in_week_year** | **int** |  | [optional] 
**zone_shared** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


